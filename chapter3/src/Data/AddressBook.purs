module Data.AddressBook where

import Prelude

import Control.Plus (empty)
import Data.List (List(..), filter, head, null, nubBy)
import Data.Maybe (Maybe)

type Entry =
 { firstName :: String
 , lastName :: String
 , address :: Address
 }

type Address =
 { street :: String
 , city :: String
 , state :: String
 }

type AddressBook = List Entry

showEntry :: Entry -> String
showEntry entry = entry.lastName <> ", " <>
 entry.firstName <> ": " <>
 showAddress entry.address

showAddress :: Address -> String
showAddress addr = addr.street <> ", " <>
 addr.city <> ", " <>
 addr.state

emptyBook :: AddressBook
emptyBook = empty

insertEntry :: Entry -> AddressBook -> AddressBook
insertEntry = Cons

findEntry :: String -> String -> AddressBook -> Maybe Entry
findEntry firstName lastName = head <<< filter filterEntry
 where
  filterEntry :: Entry -> Boolean
  filterEntry entry = entry.firstName == firstName && entry.lastName == lastName

printEntry :: String -> String -> AddressBook -> Maybe String
printEntry firstName lastName book = map showEntry (findEntry firstName lastName book)

findEntryAddress :: String -> AddressBook -> Maybe Entry
findEntryAddress streetAddress = head <<< filter filterEntry
 where
  filterEntry :: Entry -> Boolean
  filterEntry entry = entry.address.street == streetAddress

findEntryIsNull :: String -> String -> AddressBook -> Boolean
findEntryIsNull firstName lastName = null <<< filter filterEntry
 where
  filterEntry :: Entry -> Boolean
  filterEntry entry = entry.firstName == firstName && entry.lastName == lastName

entryExists :: String -> String -> AddressBook -> Boolean
entryExists firstName lastName book = false == findEntryIsNull firstName lastName book

removeDuplicates :: AddressBook -> AddressBook
removeDuplicates book = nubBy (isDuplicate) book
 where
  isDuplicate :: Entry -> Entry -> Boolean
  isDuplicate entry1 entry2 = entry1.firstName == entry2.firstName && entry1.lastName == entry2.lastName
