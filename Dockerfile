# Get the initial image
FROM centos:7
MAINTAINER Phillip Rak <rak.phillip@gmail.com>

# Update the box
RUN yum update -y

# Install wget
RUN yum -y install wget git

# Install nodejs and build tools
RUN curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
RUN yum -y install nodejs
RUN yum install gcc-c++ make -y

# Install pulp and bower
RUN npm install -g pulp bower

# Install purescript
RUN cd /opt \
    && wget https://github.com/purescript/purescript/releases/download/v0.11.7/linux64.tar.gz \
    && tar -xvf linux64.tar.gz \
    && rm /opt/linux64.tar.gz

# Install psc-package
RUN cd /opt \
    && wget https://github.com/purescript/psc-package/releases/download/v0.4.0/linux64.tar.gz \
    && tar -xvf linux64.tar.gz \
    && rm /opt/linux64.tar.gz

ENV PATH /opt/purescript:$PATH
ENV PATH /opt/psc-package:$PATH

# Create a user for working with purescript
# We are doing this because bower does not seem to play nicely with the root user
RUN useradd -m -s /bin/bash pureuser
USER pureuser

# Configure a working directory for our project
RUN mkdir -p /home/pureuser/proj
WORKDIR /home/pureuser/proj